PS1="[\[\033[35m\]\u \[\033[31m\]\h \[\033[36;1;4m\]\w\[\033[0m\]]\n\$ "
PS2="[\u \w]\$"

export MANPATH=$MANPATH:/usr/share/man/

alias ls='ls -F'
alias sl='ls -F'
alias sl2='ls -F'
alias ll='ls -lF'
alias llt='ll -t'
alias lls='ll -rSh'
alias llh='ll -h'
alias llr='ll -rt'
#alias lsd='find . -type d -depth 1'
alias lsd='ls | grep /'

alias w='w | sort'
alias l=ll
alias cdo='cd $OLDPWD'
alias dfh='df -h'

alias llt='ll -t'
alias lt='ll -rt'
alias llh='ll -h'
alias rmb='rm *~'

alias grep='grep --colour=auto'

alias gcc99='gcc -std=c99'

alias delcom='sed -e "/^#/d; /^$/d;"'

alias lsip='lsof -i -n -P'

alias v="vim"
alias gs="git status"
alias gc="git commit -m"
alias gd="git diff"
alias ga="git add"


alias sstart="sudo systemctl start"
alias sstop="sudo systemctl stop"
alias senable="sudo systemctl enable"
alias sreload="sudo systemctl reload"
alias srestart="sudo systemctl restart"

# memo : systemctl の起動スクリプトパス:　/etc/systemd/system/

# ディレクトリのみを表示する
# find . -type d

# http://blog.kaburk.com/os/linux/bash-history-sharing.html#extended
#function share_history {  # 以下の内容を関数として定義
#history -a  # .bash_historyに前回コマンドを1行追記
#history -c  # 端末ローカルの履歴を一旦消去
#history -r  # .bash_historyから履歴を読み込み直す
#}
#PROMPT_COMMAND='share_history'  # 上記関数をプロンプト毎に自動実施
shopt -u histappend   # .bash_history追記モードは不要なのでOFFに
export HISTSIZE=99999  # 履歴のMAX保存数を指定 

HISTSIZE=1000000
HISTFILESIZE=1000000
HISTCONTROL=ignoredups
export HISTSIZE HISTFILESIZE HISTCONTROL
shopt -s no_empty_cmd_completion

HISTTIMEFORMAT='[%Y-%m-%d %T] '; export HISTTIMEFORMAT

# XOFF,XONを無効化 (C-s, C-q)
stty start undef
# disable C-s
if [ -t 0 ]; then
	stty stop undef
fi

bind 'set completion-ignore-case on'
shopt -s no_empty_cmd_completion


# alias vim="vim -T xterm-256color"

alias vim=\vim
export EDITOR=vim

export LANG=ja_JP.UTF-8
export PAGER="lv -c "
export TERM=xterm-256color

# export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig/"


if [ -f /usr/local/etc/bash_completion ]; then
        . /usr/local/etc/bash_completion
fi
 
export PATH="/home/isucon/local/perl/bin/:$PATH:/usr/share/memcached/scripts/"


