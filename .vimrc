set fileencodings=utf8,euc-jp,cp932,sjis,iso-2022-jp,latin1
set fileformats=unix,dos,mac
set autoindent
set smartindent
set smarttab

set tabstop=4
"set expandtab
set shiftwidth=4
set shiftround

" " colorscheme adrian
" " colorscheme 256-jungle
"colorscheme desert
"set background=dark
"colorscheme evening
" set hlsearch
" 
" set smarttab
" 
" set tabstop=4
" set expandtab
" set shiftwidth=4
" set shiftround
" 
" 



" dein.vimの設定(NeoBundleの替わり）

"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/home/isucon/.vim/bundles/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/home/isucon/.vim/bundles')
  call dein#begin('/home/isucon/.vim/bundles')

  " Let dein manage dein
  " Required:
  call dein#add('/home/isucon/.vim/bundles/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here:
  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')
  call dein#add('tpope/vim-fugitive')
  call dein#add('kien/ctrlp.vim')
  call dein#add('flazz/vim-colorschemes')
  call dein#add('Shougo/neocomplete')
  call dein#add('vim-scripts/nginx.vim')
  " yaml
  call dein#add('vim-scripts/yaml')
  " sql
  call dein#add('vim-scripts/sql')



  " You can specify revision/branch/tag.
  call dein#add('Shougo/vimshell', { 'rev': '3787e5' })

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
"if dein#check_install()
"  call dein#install()
"endif

"End dein Scripts-------------------------


""""neocompleteの設定
"""if neobundle#is_installed('neocomplete')
    " neocomplete用設定
    let g:neocomplete#enable_at_startup = 1
    let g:neocomplete#enable_ignore_case = 1
    let g:neocomplete#enable_smart_case = 1
    if !exists('g:neocomplete#keyword_patterns')
        let g:neocomplete#keyword_patterns = {}
    endif
    let g:neocomplete#keyword_patterns._ = '\h\w*'
"""endif


" モード群
"au BufNewFile,BufRead *.yaml,*.yml so ~/.vim/yaml.vim
"au BufNewFile,BufRead *.html,*.xml so ~/.vim/xml.vim
"au BufNewFile,BufRead *.tt set filetype=html
"au BufNewFile,BufRead *.psgi set filetype=perl


" モード群
" au BufNewFile,BufRead *.yaml,*.yml so ~/.vim/yaml.vim
au FileType yaml set expandtab ts=2 sw=2 enc=utf-8 fenc=utf-8
" au BufNewFile,BufRead *.tt,*.xt set filetype=xml
" au BufNewFile,BufRead *.tt,*.xt,*.tx,*.html so ~/.vim/xml.vim
au BufNewFile,BufRead *.tt,*.xt,*.tx,*.html set filetype=html expandtab
au BufNewFile,BufRead *.psgi set filetype=perl
au BufRead,BufNewFile /etc/nginx/*,/etc/nginx/**/*,/usr/local/nginx/conf/* if &ft == '' | setfiletype nginx | endif 

au BufNewFile *.py :0r ~/.vim/snippet/python | :echo "snippet read."
au BufNewFile *.pl,*.pm :0r ~/.vim/snippet/perl | :echo "snippet read."



" 文字コードの自動認識
if &encoding !=# 'utf-8'
  set encoding=japan
  set fileencoding=japan
endif
if has('iconv')
  let s:enc_euc = 'euc-jp'
  let s:enc_jis = 'iso-2022-jp'
  " iconvがeucJP-msに対応しているかをチェック
  if iconv("\x87\x64\x87\x6a", 'cp932', 'eucjp-ms') ==# "\xad\xc5\xad\xcb"
    let s:enc_euc = 'eucjp-ms'
    let s:enc_jis = 'iso-2022-jp-3'
  " iconvがJISX0213に対応しているかをチェック
  elseif iconv("\x87\x64\x87\x6a", 'cp932', 'euc-jisx0213') ==# "\xad\xc5\xad\xcb"
    let s:enc_euc = 'euc-jisx0213'
    let s:enc_jis = 'iso-2022-jp-3'
  endif
  " fileencodingsを構築
  if &encoding ==# 'utf-8'
    let s:fileencodings_default = &fileencodings
    let &fileencodings = 'utf-8,'.s:enc_jis .','. s:enc_euc .',cp932'
    let &fileencodings = &fileencodings .','. s:fileencodings_default
    unlet s:fileencodings_default
  else
    let &fileencodings = &fileencodings .','. s:enc_jis
    set fileencodings+=utf-8,ucs-2le,ucs-2
    if &encoding =~# '^\(euc-jp\|euc-jisx0213\|eucjp-ms\)$'
      set fileencodings+=cp932
      set fileencodings-=euc-jp
      set fileencodings-=euc-jisx0213
      set fileencodings-=eucjp-ms
      let &encoding = s:enc_euc
      let &fileencoding = s:enc_euc
    else
      let &fileencodings = &fileencodings .','. s:enc_euc
    endif
  endif
  " 定数を処分
  unlet s:enc_euc
  unlet s:enc_jis
endif
" 日本語を含まない場合は fileencoding に encoding を使うようにする
if has('autocmd')
  function! AU_ReCheck_FENC()
    if &fileencoding =~# 'iso-2022-jp' && search("[^\x01-\x7e]", 'n') == 0
      let &fileencoding=&encoding
    endif
  endfunction
  autocmd BufReadPost * call AU_ReCheck_FENC()
endif
" 改行コードの自動認識
set fileformats=unix,dos,mac
" □とか○の文字があってもカーソル位置がずれないようにする
if exists('&ambiwidth')
  set ambiwidth=double
endif


set showcmd
set showmatch
set showmode
set matchtime=3
set laststatus=2
set statusline=%<%f\ %m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=%l,%c%V%8P"
set smartindent
"カーソルキーで行末／行頭の移動可能に設定
set whichwrap=b,s,[,],<,>
"コマンドライン補完するときに強化されたものを使う
set wildmenu
set wildmode=longest,list,full
"インクリメンタルサーチ
set incsearch
"検索文字の強調表示
set hlsearch

" set cursorline
set nocompatible        " Use Vim defaults (much better!)
set bs=indent,eol,start         " allow backspacing over everything in insert mode
"set ai                 " always set autoindenting on
"set backup             " keep a backup file
set viminfo='20,\"50    " read/write a .viminfo file, don't store more
                        " than 50 lines of registers
set history=50          " keep 50 lines of command line history
set ruler               " show the cursor position all the time

set mouse=a
set ttymouse=xterm2


"""""""""""""""""""""""""""""""""""""""""""""""""""
" インサートモードで、一部emacs風な操作ができるようにする
" C-p, C-nは補完に割り当てられているからどうしようかなぁ
inoremap <C-a> <HOME>
inoremap <C-e> <END>
inoremap <C-b> <LEFT>
inoremap <C-f> <RIGHT>
inoremap <C-k> <ESC>ld$A
inoremap <C-s> <Esc>/
inoremap <C-y> <ESC>pa
inoremap <C-d> <DELETE>
inoremap <C-o> <END><RETURN>

" 補完は変えたくないけど、、、仕方ないので、Ctrl+Shiftに設定する
inoremap <C-S-p> <C-p>
inoremap <C-S-n> <C-n>
inoremap <C-p> <UP>
inoremap <C-n> <DOWN>
"""""""""""""""""""""""""""""""""""""""""""""""""""


"""" キーマッピング
nnoremap <F2> <Esc>:call ToggleCursorLine()<CR>
inoremap <F2> <Esc>:call ToggleCursorLine()<CR>a
nnoremap <F3> <Esc>:call TogglePaste()<CR>
inoremap <F3> <Esc>:call TogglePaste()<CR>a
inoremap <F5> sub  {<RETURN>my ($self)  = @_;<RETURN>}<UP><UP><RIGHT><RIGHT><RIGHT>


" #で行頭に戻るのを防ぐ
inoremap # ,<BS>#

" カッコ補完
inoremap {<Enter> {}<Left><CR><ESC><S-o>
inoremap [<Enter> []<Left><CR><ESC><S-o>
inoremap (<Enter> ()<Left><CR><ESC><S-o>

" jsdoc
" nmap <silent> <C-l> <Plug>(jsdoc)
let g:jsdoc_default_mapping = 0
nnoremap <silent> <C-l> :JsDoc<CR>

" 関数一覧

function! ToggleCursorLine()
        if &cursorline != ''
                set nocursorline
                echo "cursorline Off"
        else
                set cursorline
                echo "cursorline On"
        endif
endfunction

function! TogglePaste()
        if &paste != ''
                set nopaste
                echo "paste Off"
        else
                set paste
                echo "paste On"
        endif
endfunction


"if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
"  set fileencodings=utf-8,latin1
"endif


" Only do this part when compiled with support for autocommands
if has("autocmd")
  augroup redhat
    " In text files, always limit the width of text to 78 characters
    autocmd BufRead *.txt set tw=78
    " When editing a file, always jump to the last cursor position
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line ("'\"") <= line("$") |
    \   exe "normal! g'\"" |
    \ endif
  augroup END
endif

if has("cscope") && filereadable("/usr/bin/cscope")
   set csprg=/usr/bin/cscope
   set csto=0
   set cst
   set nocsverb
   " add any database in current directory
   if filereadable("cscope.out")
      cs add cscope.out
   " else add database pointed to by environment
   elseif $CSCOPE_DB != ""
      cs add $CSCOPE_DB
   endif
   set csverb
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

if &term=="xterm"
     set t_Co=8
     set t_Sb=m
     set t_Sf=m
endif
set tabstop=4

set shiftwidth=4

" buffer idou
nmap bb :ls<CR>:buf
" gtags
    " 検索結果Windowを閉じる
    nnoremap <C-q> <C-w><C-w><C-w>q
    " Grep 準備
    nnoremap <C-g> :Gtags -g
    " このファイルの関数一覧
    " nnoremap <C-l> :Gtags -f %<CR>
    " カーソル以下の定義元を探す
    nnoremap <C-j> :Gtags <C-r><C-w><CR>
    " カーソル以下の使用箇所を探す
    nnoremap <C-k> :Gtags -r <C-r><C-w><CR>
    " 次の検索結果
    nnoremap <C-n> :cn<CR>
    " 前の検索結果
    nnoremap <C-p> :cp<CR>

highlight Comment  ctermfg=Blue
"highlight Comment  ctermfg=Cyan

